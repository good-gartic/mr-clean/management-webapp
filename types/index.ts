export type ParsedRuleFilter = {
    allowed: Array<String>,
    denied: Array<String>,
    defaultAllowed: boolean
}

export type ParsedRule = {
    id: number,
    pattern: string,
    channelsFilter: ParsedRuleFilter,
    rolesFilter: ParsedRuleFilter,
    usersFilter: ParsedRuleFilter,
    repostChannelId: string,
    enabled: boolean,
    delay: number
};