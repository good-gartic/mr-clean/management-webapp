import { ParsedRuleFilter } from "../types";

export const separator = ";";

export const negation = "~";

export const parseRuleFilter = (raw: string): ParsedRuleFilter => {
   const items = raw.split(separator);

   const allowed = items.filter(id => !id.startsWith(negation));
   const denied = items.filter(id => id.startsWith(negation)).map(id => id.replace(negation, "~"));

   return {
    allowed: allowed,
    denied: denied,
    defaultAllowed: allowed.length === 0
   };
};