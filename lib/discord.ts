import { REST } from "@discordjs/rest";

const token = process.env.DISCORD_TOKEN;

if (!token) {
    throw "The environment variable DISCORD_TOKEN is not set!";
}

export const api = new REST({ version: "10" }).setToken(token);
export const guild = process.env.DISCORD_GUILD_ID ?? "683633975838769192";