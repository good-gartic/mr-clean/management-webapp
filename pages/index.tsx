import { Button, Container, CssVarsProvider, Typography } from "@mui/joy";
import Head from "next/head";
import styled from "@emotion/styled";

const Header = styled.header`
  padding: 3rem 0;
  text-align: center;
`;

export default function Home() {
  return (
    <CssVarsProvider>
      <Head>
        <title>Mr. Clean</title>
        <meta
          name="description"
          content="Management panel for the Mr. Clean bot"
        />
        <meta
          name="og:image"
          content="https://gitlab.com/uploads/-/system/group/avatar/62069132/mr-clean.png?width=128"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>

      <Container>
        <Header>
          <nav className="my-5">
            <Typography level="h2" component="h1">
              Mr. Clean dashboard
            </Typography>
          </nav>
        </Header>
      </Container>
    </CssVarsProvider>
  );
}
