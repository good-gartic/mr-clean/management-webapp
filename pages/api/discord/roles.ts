import type { NextApiRequest, NextApiResponse } from "next";
import { api, guild } from "../../../lib/discord";
import { Routes, APIGuildCreateRole} from "discord-api-types/v10";

type Roles = Array<APIGuildCreateRole>;

const fetchRoles = async (): Promise<Roles> => {
  return await api.get(Routes.guildRoles(guild)) as Roles;
};

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  if (request.method !== "GET") {
    response.status(400);
    response.end();
    return;
  }

  const raw = await fetchRoles();
  const mapped = raw
    .map((role) => {
      return {
        id: role.id,
        name: role.name,
        color: role.color
      };
    });

  response.json(mapped);
};

export default handler;
