import type { NextApiRequest, NextApiResponse } from "next";
import { api, guild } from "../../../lib/discord";
import { Routes, ChannelType, APIGuildChannel } from "discord-api-types/v10";

type TextChannels = Array<APIGuildChannel<ChannelType.GuildText>>;

const fetchChannels = async (): Promise<TextChannels> => {
  const all = (await api.get(Routes.guildChannels(guild))) as TextChannels;
  return all.filter((channel) => channel.type === ChannelType.GuildText);
};

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  if (request.method !== "GET") {
    response.status(400);
    response.end();
    return;
  }

  const raw = await fetchChannels();
  const mapped = raw
    .sort((a, b) => a.position - b.position)
    .map((channel) => {
      return {
        id: channel.id,
        name: channel.name,
      };
    });

  response.json(mapped);
};

export default handler;
