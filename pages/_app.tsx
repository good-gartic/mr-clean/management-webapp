import type { AppProps } from "next/app";
import { withPasswordProtect } from "next-password-protect";

const App = ({ Component, pageProps }: AppProps) => {
  return <Component {...pageProps} />;
};

export default withPasswordProtect(App, {
  loginApiUrl: "/api/login",
  checkApiUrl: "/api/password",
});
